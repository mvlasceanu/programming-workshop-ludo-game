/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    tests
 * @class      TestArea
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package tests;

import dk.itu.pw.ludo.ludoboard.Area;
import dk.itu.pw.ludo.ludoboard.GameManager;
import dk.itu.pw.ludo.ludoboard.Player;
import java.awt.Color;
import org.junit.Test;
import org.testng.Assert;

/**
 *
 * @author Rembrandt
 */
public class TestArea {
    
    // Hold game manager instance
    private final GameManager Manager;
    
    /**
     * Constructor
     */
    public TestArea() {
        this.Manager = new GameManager();
        this.Manager.generateBoard();
    }
    
    /**
     * New instance test
     */
    @Test
    public void newInstance()
    {
        // Color instance test
        Area area = new Area(Color.RED, this.Manager);
        Assert.assertNotNull(area);
        
        // Player instance test
        Player player = new Player("Mihai", Color.BLUE);
        Area pArea = new Area(player, this.Manager);
        Assert.assertNotNull(pArea);
    }
    
    /**
     * Field generation test
     */
    @Test
    public void generateAreaFields()
    {
        Area area = new Area(Color.RED, this.Manager);
        area.generateAreaFields(Manager);
        Assert.assertNotNull(area.getFields());
        
    }
    
    /**
     * Find next area test
     */
    @Test
    public void getNextArea()
    {
        Area area = this.Manager.getAreas().get(0);
        Assert.assertNotNull(area.getNextArea());
    }
    
    /**
     * Find Next area with player on it
     */
    @Test
    public void getNextAreaWithPlayer()
    {
        this.Manager.setUserTotalPlayers(3);
        Area area = this.Manager.getAreas().get(0);
        this.Manager.generateComputerPlayers();
        // Set 3 players (1 human and 2 robots)
        Assert.assertTrue(this.Manager.getPlayers().size() >= 2);
        Assert.assertNotNull(area.getNextAreaWithPlayer());
    }
}
