/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    tests
 * @class      TestDie
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package tests;

import dk.itu.pw.ludo.ludoboard.Die;
import dk.itu.pw.ludo.ludoboard.Player;
import java.awt.Color;
import org.junit.Test;
import org.testng.Assert;

/**
 *
 * @author Rembrandt
 */
public class TestDie {
    
    public TestDie() {
    }
    
    @Test
    public void roll()
    {
        Player player   = new Player("Mihai", Color.RED);
        Die die         = new Die();
        Assert.assertTrue(die.roll(player) > 0);
    }
}
