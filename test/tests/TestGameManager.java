/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    tests
 * @class      TestGameManager
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package tests;

import dk.itu.pw.ludo.ludoboard.Area;
import dk.itu.pw.ludo.ludoboard.ComputerPlayer;
import dk.itu.pw.ludo.ludoboard.Field;
import dk.itu.pw.ludo.ludoboard.GameManager;
import dk.itu.pw.ludo.ludoboard.Pin;
import dk.itu.pw.ludo.ludoboard.Player;
import java.awt.Color;
import java.util.ArrayList;
import org.junit.Test;
import org.testng.Assert;

/**
 *
 * @author Rembrandt
 */
public class TestGameManager {
    // Hold game manager instance
    private final GameManager Manager;
    
    /**
     * Constructor 
     */
    public TestGameManager()
    {
        this.Manager = new GameManager();
        this.getManager().generateBoard();
    }
    
    /**
     * Getter for the game manager
     * @return 
     */
    public final GameManager getManager()
    {
        return this.Manager;
    }
    
    /**
     * Test the init method
     */
    @Test()
    public void init()
    {
        // Make sure that there are 4 areas
        Assert.assertEquals(4, this.getManager().getAreas().size());
        // And make sure that there are 4 areas and 4 colors
        Assert.assertEquals(this.getManager().getAreas().size(), GameManager.getGameColors().size());
    }
    
    /**
     * Test for human player generator
     */
    @Test()
    public void testGenerateHumanPlayer()
    {
        // Create a new player
        Player P = new Player("Mihai", Color.RED);
        // Make sure the player is not null
        Assert.assertNotNull(P);
        // Add the player to the game manager
        this.getManager().addPlayer(P);
        // Make sure the pkayer was added
        Assert.assertTrue(this.getManager().getPlayers().size() == 1);
        // Assign an area to the player
        P.assignArea(this.getManager());
        // Make sure that the area was assigned
        Assert.assertTrue(P.getArea() instanceof Area);
        // Create player's pins
        P.createPins();
        // Make sure all the 4 pins were created
        Assert.assertTrue(P.getPins().size() == 4);
    }
    
    /**
     * Robot player generator
     */
    @Test
    public void generateComputerPlayers()
    {
        // Get game manager
        GameManager game = this.getManager();
        // Set 3 players (1 human and 2 robots)
        game.setUserTotalPlayers(3);
        // Generate the computer players
        boolean gcp = game.generateComputerPlayers();
        // Make sure the computer players were created
        Assert.assertTrue(gcp);
        // Get all the players from the manager
        ArrayList<Player> players = this.getManager().getPlayers();
        // Init a variable and try to find an instance of a robot player
        // i.e. ComputerPlayer
        boolean found = false;
        for(Player p : players)
        {
            if(p instanceof ComputerPlayer)
                found = true;
        }
        Assert.assertEquals(true, found);
    }
    
    /**
     * Tests that changing pin positions actually works
     */
    @Test
    public void setPinPosition()
    {
        // Generate some players
        this.generateComputerPlayers();
        // Get 1 pin from the first player
        Pin pin         = this.getManager().getPlayers().get(0).getPins().get(0);
        // Get the size of the pin's fields number
        int pathSize    = pin.getPath().size();
        // Generate a random index value for he path
        int randomIndex = (int)(Math.random() * (pathSize - 1));
        // Get the field with that random index
        Field field     = pin.getPath().get(randomIndex);
        // Try to place the pin there
        Assert.assertTrue(this.getManager().setPinPosition(field, pin));
    }
}
