/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    tests
 * @class      TestPin
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package tests;

import dk.itu.pw.ludo.ludoboard.GameManager;
import dk.itu.pw.ludo.ludoboard.Pin;
import dk.itu.pw.ludo.ludoboard.Player;
import java.awt.Color;
import junit.framework.Assert;
import org.junit.Test;


/**
 *
 * @author Rembrandt
 */
public class TestPin {
    // Hold game manager instance
    private final GameManager Manager;
    
    public TestPin() {
        this.Manager = new GameManager();
        this.Manager.generateBoard();
    }
    
    /**
     * Make sure that there is an actual path for pins
     */
    @Test
    public void generatePath()
    {
        Player player = new Player("Mihai", Color.RED);
        player.assignArea(this.Manager);
        Pin pin = new Pin(player);
        
        Assert.assertTrue(pin.getPath().size() > 0);
    }
    
}
