/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    tests
 * @class      TestPlayer
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */
package tests;
import dk.itu.pw.ludo.ludoboard.GameManager;
import dk.itu.pw.ludo.ludoboard.Pin;
import dk.itu.pw.ludo.ludoboard.Player;
import java.awt.Color;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Rembrandt
 */
public class TestPlayer {
    
    public TestPlayer() {
    }
    
    @Test
    public void Player()
    {
        Player player = new Player("Mihai", Color.RED);
        Assert.assertNotNull(player);
    }
    
    @Test
    public void createPins()
    {
        GameManager Manager = new GameManager();
        Manager.generateBoard();
        Player player = new Player("Mihai", Color.RED);
        player.assignArea(Manager);
        // Create pin 1
        Pin pin1 = new Pin(player);
        player.getPins().add(pin1);

        Assert.assertTrue(player.getPins().size() == 1);
    }
}
