/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    general
 * @class      Helper
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package general;

import dk.itu.pw.ludo.ludoboard.GameManager;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.MissingFormatArgumentException;

/**
 * Helper class for various things
 * 
 * @author Mihai-Marius Vlăsceanu
 */
public class Helper {
    
    /**
     * Matches the color's hashcode to an actual color name
     * 
     * @param hashcode
     * @return 
     */
    public static String colorHashCodeToName(int hashcode)
    {
        switch(hashcode)
        {
            case -65536:
                return Const.COLOR_LBL_RED;
            case -16776961:
                return Const.COLOR_LBL_BLUE;
            case -16711936:
                return Const.COLOR_LBL_GREEN;
            case -256 :
                return Const.COLOR_LBL_YELLOW;
        }
        return "";
    }
    
    /**
     * Displays color options to the player at the beginning of the
     * game
     * 
     * @param m
     * @return 
     */
    public static String displayColorOptions(GameManager m)
    {
        try
        {
            String options = "";
            ArrayList<Color> av_colors = m.getAvailableColors();
            ArrayList<Color> colors    = GameManager.getGameColors();
            for(int i = 0; i < colors.size(); i++)
            {
                if(av_colors.contains(colors.get(i)))
                {
                    int visible_option_id = i + 1;
                    options += "" + visible_option_id + " => " + Helper.colorHashCodeToName(colors.get(i).hashCode());
                    if(i != (colors.size() - 1))
                    {
                        options += ", ";
                    }
                }
            }
            return options;
        } catch(Exception e)
        {
            Helper.m(e.getMessage());
        }
        return null;
    }
    
    /**
     * Checks if the selected color is valid
     * 
     * @param i
     * @return 
     */
    public static boolean isValidColorId(int i)
    {
        return (i >= 0 
                && i < GameManager.getGameColors().size() 
                && GameManager.getGameColors().get(i) != null);
    }
    
    /**
     * Shortcut for System.out.println
     * @param txt 
     */
    public static void m(String txt)
    {
        System.out.println(txt);
    }
    
    /**
     * Shortcut for System.out.format
     * 
     * @param message
     * @param args 
     */
    public static void f(String message, Object... args)
    {
        System.out.format(message + "\n", args);
    }
    
    /**
     * Shortcut for String.format
     * 
     * @param message
     * @param args 
     * @return  
     */
    public static String sf(String message, Object... args)
    {
        return String.format(message, args);
    }

    public static void m(StringBuilder output) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public static Object ask(String m, String type, Object... args) throws IOException, NumberFormatException, MissingFormatArgumentException, ClassCastException
    {
        try
        {
            // Init input window
            InputStreamReader input = new InputStreamReader(System.in);
            // Store the types stuff in memory
            BufferedReader reader = new BufferedReader(input);
            // Ask
            Helper.f(m, args);
            
            switch(type)
            {
                case "INT": return (int) Integer.parseInt(reader.readLine());
                case "STRING": return (String) reader.readLine();
                default: return reader.readLine();
            }
        } catch(IOException | NumberFormatException | MissingFormatArgumentException | ClassCastException e)
        {
            Helper.m(e.getMessage());
        }
        return m;
    }
}
