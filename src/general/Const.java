/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    general
 * @class      Const
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package general;

public class Const {
    // System
    public static String NEW_LINE           = System.getProperty("line.separator");
    
    public static String GAME_NAME          = "Ludo";
    public static String COLOR_LBL_RED      = "Red";
    public static String COLOR_LBL_BLUE     = "Blue";
    public static String COLOR_LBL_YELLOW   = "Yellow";
    public static String COLOR_LBL_GREEN    = "Green";
    public static String PLAYER_PINS_LEFT   = "Player %s has %d pins left in the game!";
    public static String PLAYER_WON         = "Player %s has won the game!!!";
    public static String PLAYER_FINISHED    = "Player %s has finished the game!!!";
    
    // User interaction
    public static String BOARD_PLAYERS_HOW_MANY    = "How many players want to play? (2 to 4) \n";
    public static String BOARD_PLAYERS_Q_GETNAMES  = "Player #%d name: \n";
    public static String BOARD_PLAYERS_Q_COLORS    = "Player #%d color (%s) \n";
    public static String GM_COLOR_ALREADY_CHOSEN   = "That color was already picked by %s. Please pick another one.\n";
    public static String START_WHAT_TO_DO          = "Game Menu: \n";
    public static String MENY_START                = "%d. Start a New Gamne \n";
    public static String MENU_EXIT                 = "%d. Exit";
    public static String BYE_MESSAGE               = "Goodbye!";
    public static final int MENU_GAME_START        = 1;
    public static final int MENU_QUIT              = 0;
    public static String PLAYER_THREW_DIE_6_MSG    = "%s threw the die and got a %d. \n %s will now start the game.\n";
    public static String PLAYER_THREW_DIE          = "%s threw the die and got a %d. \n";
    public static String PLAYER_NEEDED_6           = "%s can't move any pins  because he/she needed a 6, he/she threw a %d. \n";
    public static String PLAYER_STARTS_GAME        = "%s starts the game now because he/she threw the biggest die.";
    public static String GET_PIN_OUT               = "Get pin out!";
    public static String WELCOME                   = "Welcome to the %s ! \n Press OK to start a new game or cancel to exit.";
    public static String PLAYER_CAN_MOVE_POS       = "%s has the following moving options:";
    public static String OPPONENT_BEHIND           = "%s will have Pin #%s %d fields behind";
    public static String OPPONENT_IN_FRONT         = "%s will have Pin #%s %d fields away";
    public static String YOUR_PIN_BEHIND           = "Your #%s pin will be %d fields behind";
    public static String YOUR_PIN_IN_FRONT         = "Your #%s pin will be %d fields away";
    public static String PLAYER_CANT_MOVE          = "%s cannot move any pin.";
    public static String KILL_ENEMY                = "Kill %s's #%s pin !";
    public static String PIN_KILLED                = "%s's #%s pin was killed by %s";
    
    
    // Exception messages
    public static String EXC_INVALID_COLOR_SELECTION= "The option you have selected is not a valid color number. Pleas try again.";
    public static String EXC_INVALID_PLAYERS_NUMBER = "The number you have selected is not valid or is not a number at all!";
    public static String EXC_SELECTION_DNE          = "The selected option doesn't exist. Please try again!";
    
    // Log messages
    public static String LOG_PIN_MOVES              = "%s moved his/her #%s pin from %s field to %s field (%s field).";
    public static String LOG_PIN_MOVE_NCP           = "%s moved his/her #%s pin to %s field (%s field).";
}
