/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.pw.ludo.ludoboard
 * @class      Pin
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.pw.ludo.ludoboard;

import general.Const;
import general.Helper;
import java.util.ArrayList;

public class Pin {
    // Instance counter
    public static int COUNT_PIN_INSTANCES = 0;
    // Unique pin id
    private String id;
    // Owner of the pin
    private Player owner;
    // Path for this pin
    private final ArrayList<Field> path = new ArrayList<>();

    
    /**
     * Constructor
     * @param p 
     */
    public Pin(Player p)
    {
        this.owner = p;
        this.generateUniqueId();
        this.generatePath();
    }
    
    /**
     * Getter for pin id
     * @return 
     */
    public String getId()
    {
        return this.id;
    }
    
    /**
     * Setter for the id
     * @param id 
     */
    private void setId(String id)
    {
        try {
            this.id = id;
        } catch(Exception E)
        {
            Helper.m(E.getMessage());
        }
        
    }
    
    /**
     * Generate a unique id for the pin
     */
    private void generateUniqueId()
    {
        int counter = 1;
        
        for(Position pos : this.getManager().getPinPositions())
        {
            if(pos.getPin().getOwner().equals(this.getOwner()))
                counter++;
        }
        String pid = this.getArea().getId() + "" + counter;
        this.setId(pid);
    }
    
    /**
     * Set the owner of the pin
     * 
     * @param p 
     */
    public void setOwner(Player p)
    {
        this.owner = p;
    }
    
    /**
     * Retrieve pin's player
     * 
     * @return 
     */
    public Player getOwner()
    {
        return this.owner;
    }
    
    /**
     * Returns pin's area, in accordance with its' owner
     * 
     * @return 
     */
    public Area getArea()
    {
        return this.getOwner().getArea();
    }
    /**
     * Get game mamanger
     * @return 
     */
    public GameManager getManager()
    {
        return this.getArea().getManager();
    }
    
    /**
     * Moves a pin to the home field
     */
    public final void toHomeField()
    {
        ArrayList<Field> homeFields = this.getArea().getHomeFields();
        for(Field f : homeFields)
        {
            if(!f.isOccupied())
            {
                GameManager Manager = this.getManager();
                Manager.setPinPosition(f, this, false);
                break;
            }
        }
    }
    
    /**
     * Moves a pin to the home field
     */
    public final void killed()
    {
        ArrayList<Field> homeFields = this.getArea().getHomeFields();
        for(Field f : homeFields)
        {
            if(!f.isOccupied())
            {
                GameManager Manager = this.getManager();
                Manager.setPinPosition(f, this, true);
                break;
            }
        }
    }
    
    /**
     * It generates one single path that the pin can follow
     * on the board
     */
    public final void generatePath()
    {
        // Get pin's area
        Area home_area = this.getArea();
        // Get the fields in the area
        ArrayList<Field> fields = home_area.getFields();
        // Loop through all the fields
        for(Field f : fields)
        {
            // Check the field type
            switch(f.getFieldType())
            {
                // If the field is a start field or a game field
                case START:
                case GAME:
                    // Add it tothe path
                    this.getPath().add(f);
                    break;
            }
        }
        
        // Get the next area to follow on the track
        Area second_area = home_area.getNextArea();
        // Get its' fields
        ArrayList<Field> second_area_fields = second_area.getFields();
        // Loop thorugh its' fields
        for(Field saf : second_area_fields)
        {
            // Check the field's type
            switch(saf.getFieldType())
            {
                // If the field is a game field
                case CORNER:
                case GAME:
                    // Add it to the path
                    this.getPath().add(saf);
                    break;
            }
        }
        
        // Get the 3rd area to follow on the board
        Area third_area = second_area.getNextArea();
        // Get its' fields
        ArrayList<Field> third_area_fields = third_area.getFields();
        // Loop through fields
        for(Field taf : third_area_fields)
        {
            // Check field type
            switch(taf.getFieldType())
            {
                // If it is a game field, add it to the path
                case CORNER:
                case GAME:
                    this.getPath().add(taf);
                    break;
            }
        }
        
        // Get the 3rd area to follow on the board
        Area fourth_area = third_area.getNextArea();
        // Get its' fields
        ArrayList<Field> fourth_area_fields = fourth_area.getFields();
        // Loop through fields
        for(Field taf : fourth_area_fields)
        {
            // Check field type
            switch(taf.getFieldType())
            {
                // If it is a game field, add it to the path
                case CORNER:
                case GAME:
                    this.getPath().add(taf);
                    break;
            }
        }
        
        // Back to the home area
        for(Field f : fields)
        {
            // If the field type is a field within the final column path
            switch(f.getFieldType())
            {
                case COLUMN:
                    // Add it to the track
                    this.getPath().add(f);
                    break;
            }
        }
        // Aaaaand add the last field
        this.getPath().add(fields.get(fields.size() - 1));
    }
    
    /**
     * It gets the board path for this pin
     * 
     * @return 
     */
    public ArrayList<Field> getPath()
    {
        return this.path;
    }
    
    /**
     * Retrieves the playable fields on the board
     * @return 
     */
    public ArrayList<Field> getFieldsWithoutHome()
    {
        ArrayList<Field> r = this.getPath();
        ArrayList<Field> t = r;
        for (Field f : t) {
            if (FieldType.HOME == f.getFieldType() || FieldType.CORNER == f.getFieldType())
                t.remove(f);
        }
        return r;
    }
    
    /**
     * Gets the current pin's field
     * @return 
     */
    public Field getCurrentField()
    {
        ArrayList<Position> positions = this.getManager().getPinPositions();
        
        for(Position ps : positions)
        {
            if(ps.getPin() == this)
                return ps.getField();
        }
        
        return null;
    }
    
    /**
     * Retreieves the next field to move according with the die's value
     * @param die
     * @return 
     */
    public Field getNextFieldByDie(Die die)
    {
        try {
            // Current field
            Field current = this.getCurrentField();
            int currentIndex = 0;
            for(Field f : this.getPath())
            {
                if(f.getId() == null ? current.getId() == null : f.getId().equals(current.getId()))
                    currentIndex = this.getPath().indexOf(f);
            }

            // Get die value
            int dieValue     = die.getValue();
            // Next index
            int nextIndex    = currentIndex + dieValue;
            // Init next
            Field nextField;
            if(nextIndex < this.getPath().size())
            {
                nextField = this.getPath().get(nextIndex);
            } else {
                int left        = this.getPath().size() - currentIndex;
                int goBackIndex = dieValue - left;
                nextField = this.getPath().get(goBackIndex);
            }
            return nextField;
        } catch(IndexOutOfBoundsException E)
        {
            Helper.m(E.getMessage());
        }
        return null;
    }
    
    /**
     * Returns a string with the entire path of the pin
     * @return 
     */
    public String pathToString()
    {
        StringBuilder text = new StringBuilder();
        text.append("Size: ");
        text.append(this.getPath().size());
        for(Field f : this.getPath())
        {
            text.append(" [");
            text.append(f.getId());
            text.append(" - ");
            text.append(f.getFieldType());
            text.append("] \n");
        }
        
        return text.toString();
    }
    
    public Position getPosition()
    {
        Position current = null;
        
        for(Position p : this.getManager().getPinPositions())
        {
            if(p.getPin().equals(this))
                current = p;
        }
        
        return current;
    }
    
    public boolean isTheClosestToGoal()
    {
        boolean is = true;
        
        GameManager manager = this.getManager();
        ArrayList<Position> pinPositions = manager.getPinPositions();
        
        for(Position p : pinPositions)
        {
            if(!p.equals(this.getPosition()) && pinPositions.indexOf(p) > pinPositions.indexOf(this.getPosition()))
                is = false;
        }
        
        return is;
    }
    
    public String getOpponentsInProximity()
    {
        Field currentField = this.getCurrentField();
        int currentFieldIndex = this.getPath().indexOf(currentField);
        ArrayList<Position> allPositions = this.getManager().getPinPositions();
        // Init StringBuilder
        StringBuilder output = new StringBuilder();
        output.append("");
        
        for(Position p : allPositions)
        {
            Field f = p.getField();
            Pin pin = p.getPin();
            Player player = pin.getOwner();
            
            int index = this.getPath().indexOf(f);
            
            if(f.getFieldType().equals(FieldType.GAME) && !f.equals(currentField))
            {
                
                if(currentFieldIndex - index <= 6 && currentFieldIndex - index > 0)
                {
                    if(player.equals(this.getOwner()))
                    {
                        output.append(String.format(Const.YOUR_PIN_BEHIND, pin.getId(), (currentFieldIndex - index)));
                    } else {
                        output.append(String.format(Const.OPPONENT_BEHIND, player.getName(), pin.getId(), (currentFieldIndex - index)));
                    }
                }

                if(index - currentFieldIndex <= 6 && index - currentFieldIndex > 0)
                {
                    if(player.equals(this.getOwner()))
                    {
                        output.append(String.format(Const.YOUR_PIN_IN_FRONT, pin.getId(), (index - currentFieldIndex)));
                    } else {
                        output.append(String.format(Const.OPPONENT_IN_FRONT, player.getName(), pin.getId(), (index - currentFieldIndex)));
                    }
                }
            }
        }
        
        return output.toString();
    }
    
    public boolean hasStartInPath()
    {
        boolean has = false;
        for(Field f : this.getPath())
        {
            if(f.getFieldType().equals(FieldType.START))
                has = true;
        }
        return has;
    }
}
