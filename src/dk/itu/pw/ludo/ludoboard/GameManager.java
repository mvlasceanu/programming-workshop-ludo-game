/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.pw.ludo.ludoboard
 * @class      Board
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.pw.ludo.ludoboard;

import general.Const;
import general.Helper;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

/**
 * Board class
 * @author Mihai-Marius Vlăsceanu
 */
public class GameManager extends Board {
    
    // Holds all the data regarding areas (fields, players)
    private final ArrayList<Area> areas = new ArrayList<>();
    // Player turn queue
    private final ArrayList turn = new ArrayList<>();
    // Scoreboard
    private final ArrayList<Player> scoreboard = new ArrayList<>();
    // Parent board
    private Board board;
        // Hold the players
    private final ArrayList<Player> players       = new ArrayList<>();
    // Holds the positions for all the player's pins
    private final ArrayList<Position> pinPositions  = new ArrayList<>();
    // Store the number of Players the human chose to Play
    private int total_players;
    // Q-Learning settings
    private final double ALPHA      = 0.001;
    private final double GAMMA      = 0.75;
    // Number of iterations for learning
    private final int ITERATIONS    = 100000;
    // Length of the board for each pin 
    private final int BOARDLENGTH   = Board.PATH_SIZE;
    // Last Field
    private final int LASTSQUARE    = BOARDLENGTH - 1;
    // Memorize the table
    private final double LearningTable[][] = new double[BOARDLENGTH][6];
    
    /**
     * Constructor
     * 
     * Generates the board (area, colors, fields, players)
     */
    public GameManager() {
    }
    
    public final void init()
    {
        this.generateBoard();
        this.getMainMenu();

    }
    
    /**
     * Setter for player number
     * @param t 
     */
    public void setUserTotalPlayers(int t)
    {
        this.total_players = t;
    }
    
    /**
     * Getter for player number
     * @return 
     */
    public int getUserTotalPlayers()
    {
        return this.total_players;
    }
    
    /**
     * Generates the board at the beginning
     * (areas, fields, colors)
     */
    public final void generateBoard()
    {
        for (int i = 0; i < GameManager.getGameColors().size(); i++) {
                Color color = GameManager.getGameColors().get(i);
                Area area = new Area(color, this);
                area.generateAreaFields(this);
                this.addArea(area);
        }
    }
    
    /**
     * Creates a human player
     * 
     * @return 
     */
    public final boolean generateHumanPlayer() 
    {
        // Init a boolean return
        boolean result = true;
        try {
            String name = (String) Helper.ask(Const.BOARD_PLAYERS_Q_GETNAMES, "STRING", 1);
            // Get player's color
            int color_id = 4; 
            
            while(!Helper.isValidColorId(color_id))
            {
               color_id = (int)Helper.ask(Const.BOARD_PLAYERS_Q_COLORS, "INT", 1, Helper.displayColorOptions(this)) - 1;
            }
            
            // Create new Player Instance
            Player Player = new Player(name, GameManager.getGameColors().get(color_id));
            // Add them to the Game Manager
            this.addPlayer(Player);
            // Assign the corresponding area to the player
            Player.assignArea(this);
            // Create pins
            Player.createPins();
            // return a true result
            result = true;
            
        } catch (IOException ex) {
            Helper.m(Const.EXC_INVALID_PLAYERS_NUMBER);
            return this.generateHumanPlayer();
        }
        
        return result;
    }
    
    /**
     * Generates computer players
     * @return 
     */
    public boolean generateComputerPlayers()
    {
        // Init a boolean return
        boolean result = false;
        for(int i = 2; i <= this.getUserTotalPlayers(); i++)
        {
            // Random name for the computer
            String name = ComputerPlayer.getComputerPlayerName(this);
            // Get a random index for the color
            int index = new Random().nextInt(this.getAvailableColors().size());
            // Get the color with that index
            Color computer_color = this.getAvailableColors().get(index);
            // Create new ComputerPlayer Instance
            ComputerPlayer Player = new ComputerPlayer(name, computer_color);
            // Add them to the Game Manager
            this.addPlayer(Player);
            // Assign the corresponding area to the player
            Player.assignArea(this);
            // Create pins
            Player.createPins();
            // return a true result
            if(result == false) {
                result = true;
            }
                
        }
        this.learn();
        return result;
    }
    
    /**
     * It generates the players at the beginning of the game
     *  
     * @return 
     */
    public final boolean generatePlayers()
    {
        // Init a boolean return
        boolean result = false;
        try {
            // Get the input
            int pl_no   = (int) Helper.ask(Const.BOARD_PLAYERS_HOW_MANY, "INT");
            // 
            this.setUserTotalPlayers(pl_no);
            
            // Check is the user eneterd a number between 2 and 4
            if(this.isValidPlayersNumber(this.getUserTotalPlayers()))
            {
               this.generateHumanPlayer();
               this.generateComputerPlayers();
            } else {
                return this.generatePlayers();
            }
        } catch(IOException | NumberFormatException | IndexOutOfBoundsException | ClassCastException e)
        {
            Helper.m(Const.EXC_INVALID_PLAYERS_NUMBER);
            return this.generatePlayers();
        }
        return result;
    }
    
    /**
     * Get board areas
     * @return 
     */
    public final ArrayList<Area> getAreas()
    {
        return this.areas;
    }
    
    /**
     * Add an area to the current board
     * @param item
     * @return 
     */
    public ArrayList<Area> addArea(Object item)
    {
        this.getAreas().add((Area) item);
        return this.areas;
    }
    
    /**
     * Return all colors
     * 
     * @return
     */
    public static ArrayList<Color> getGameColors()
    {
       ArrayList<Color> Colors = new ArrayList<>();
       Colors.add(Color.RED);
       Colors.add(Color.BLUE);
       Colors.add(Color.GREEN);
       Colors.add(Color.YELLOW);
       return Colors;
    }
    
    /**
     * Retrieve unused colors
     * 
     * @return 
     */
    public ArrayList<Color> getAvailableColors()
    {
        ArrayList<Color> colors = GameManager.getGameColors();
        
        if(!this.getPlayers().isEmpty())
        {
            for(Player p : this.getPlayers())
            {
                colors.remove(p.getColor());
            }
        }
        return colors;
    }
    
    /**
     * Displays the state of the game (fields, pins, players)
     */
    public void displayGameState()
    {
        // Build a string
        StringBuilder console = new StringBuilder();
        // Loop through areas collection
        for(int i = 0; i < this.getAreas().size(); i++)
        {
            // get the area
            Area area = this.getAreas().get(i);
            console.append("State for the area ").append(area.getName()).append(Const.NEW_LINE);
            if(area.hasOwner())
            {
                console.append("Owner: ").append(area.getOwner().toString()).append(Const.NEW_LINE);
            }
            for(int j = 0; j < area.getFields().size(); j++)
            {
                
                console.append("Field [")
                       .append(area.getFields().get(j).getFieldType())
                       .append("]: ")
                       .append(area.getFields().get(j).getId());
                       
                for(Position p : this.getPinPositions())
                {
                    if(p.getField() == area.getFields().get(j))
                    {
                        console.append(" Pin: ")
                               .append(p.getPin().getId())
                               .append(" | Path: ")
                               .append(p.getPin().pathToString());
                    }
                }
                
                console.append(Const.NEW_LINE);
            }
            
        }
        Helper.m(console.toString());
    }
    
     /**
     * Getter for players
     * @return 
     */
    public ArrayList<Player> getPlayers()
    {
        return this.players;
    }
    /**
     * Add players to the game
     * @param p
     * @return 
     */
    public ArrayList<Player> addPlayer(Player p)
    {
        if(this.getTotalPlayers() <= 4)
        {
            this.players.add(p);
        }
        return this.getPlayers();
    }
    /**
     * Delete all players
     */
    public void dropPlayers()
    {
        this.getPlayers().clear();
    }
    
    /**
     * Get the total number of players
     * @return 
     */
    public int getTotalPlayers()
    {
        return this.getPlayers().size();
    }
    /**
     * Getter for pin positions
     * 
     * @return 
     */
    public ArrayList<Position> getPinPositions()
    {
        return this.pinPositions;
    }
    
    /**
     * Changes pin position
     * @param field
     * @param pin 
     * @param killed 
     * @return  
     */
    public boolean setPinPosition(Field field, Pin pin, boolean killed)
    {
        // Init a temporary position holder
        Position to_remove = null;
        
        // Remove an existing position of yhe pin
        for(Position p : this.getPinPositions())
        {
            if(p.getPin().getId().equals(pin.getId())) {
                to_remove = p;
            }
        }
        
        Player enemy = null;
        if(field.isOccupied())
        {
            if(!field.getOccupiedBy().equals(pin.getOwner()))
            {
                Pin occupier = field.getPins().get(0);
                enemy = occupier.getOwner();
            }
        }
        
        // Remove pin's current position
        this.getPinPositions().remove(to_remove);
        
        // Get pin owner
        Player owner = pin.getOwner();
        
        // If the field is not a goal field
        if(!field.getFieldType().equals(FieldType.GOAL)) 
        {
            // Add new field position
            this.getPinPositions().add(new Position(field, pin));
            
            if(to_remove != null)
            {
                if(!killed)
                {
                    Helper.f(Const.LOG_PIN_MOVES, 
                    owner.getName(), 
                    pin.getId(), 
                    to_remove.getField().getId(),
                    field.getId(),
                    field.getFieldType());
                } else {
                    Helper.f(Const.PIN_KILLED, 
                    enemy.getName(), 
                    pin.getId(), 
                    owner.getName());
                }
                
                return true;
            } else {
                Helper.f(Const.LOG_PIN_MOVE_NCP, 
                        owner.getName(), 
                        pin.getId(),
                        field.getId(),
                        field.getFieldType());
                return true;
            }
        } else {
            int pinsLeft = owner.getPinsLeft();
            
            if(pinsLeft > 0)
            {
                Helper.f(Const.PLAYER_PINS_LEFT, owner.getName(), pinsLeft);
                return true;
            } else {
                if(this.getScoreBoard().isEmpty())
                    Helper.f(Const.PLAYER_WON, owner.getName());
                else
                    Helper.f(Const.PLAYER_FINISHED, owner.getName());
                this.addPlayerToScoreBoard(owner);
                this.getPlayers().remove(owner);
                owner.getArea().setOwner(null);
                if(this.getPlayers().size() == 1)
                {
                    this.addPlayerToScoreBoard(this.getPlayers().get(0));
                }
                return true;
            }
        }
    }
    /**
     * Setter for board member
     * @param b 
     */
    public void setBoard(Board b)
    {
        this.board = b;
    }
    /**
     * Get board member
     * @return 
     */
    public Board getBoard()
    {
        return this.board;
    }
    /**
     * Checks if the user input for how many players
     * are in the game is a valid one (between 2 and 4)
     * 
     * @param n
     * @return 
     */
    public boolean isValidPlayersNumber(int n)
    {
        return (n <= 4 && n >= 2);
    }
    
    /**
     * Checks if a color is available for the user to chose
     * @param color_id
     * @return 
     */
    public boolean colorIsAvailable(int color_id)
    {
        for(Player p : this.getPlayers())
        {
            if(p.getColor() == GameManager.getGameColors().get(color_id))
            {
                Helper.f(Const.GM_COLOR_ALREADY_CHOSEN, p.getName());
                return false;
            }
        }
        return true;
    }
    
    /**
     * Start the game
     * @throws IOException 
     */
    public void start() throws IOException
    {

        if(this.gameCanBegin())
        {
            // Create a new Die
            Die die = new Die();

            while(this.getScoreBoard().size() < this.getUserTotalPlayers())
            {
                Player current = (this.getTurn().get(0) instanceof Player ? (Player) this.getTurn().get(0) : (ComputerPlayer) this.getTurn().get(0));
                // Throw a die
                int value = die.roll(this.getTurn().get(0));
                // Check if player can move
                if(current.canMove(die))
                {
                    // Get possible moves
                    ArrayList<Position> moptions = current.moveOptions(die);
                    if(moptions.size() > 0)
                    {
                        current.move(die);
                    } else {
                        Helper.m(current.getName() + " has no moving options!");
                        if(this.getNextPlayer() == null)
                            break;
                        this.setTurn(this.getNextPlayer());
                    }

                } else {
                    Helper.f(Const.PLAYER_CANT_MOVE, current.getName());
                    if(this.getNextPlayer() == null)
                        break;
                    this.setTurn(this.getNextPlayer());
                }
            }
            
            // Display the score board at the end
            StringBuilder scb = new StringBuilder();
            scb.append("Scoreboard: ");
            scb.append(Const.NEW_LINE);
            for(Player p : this.getScoreBoard())
            {
                int place = this.getScoreBoard().indexOf(p) + 1;
                scb.append(place);
                scb.append(": ");
                scb.append(p.getName());
                scb.append(Const.NEW_LINE);
            }
            Helper.m(scb.toString());
            
            // Display main menu
            this.getMainMenu();
        } else {
            Helper.m("The game cannot begin" + this.getAreas().size());
        }
    }
    
    /**
     * Displays a menu at the start of the game
     * @return 
     */
    public int getMainMenu()
    {
        try
        {
            // Init StringBuilder
            StringBuilder output = new StringBuilder();
            // init user input
            InputStreamReader input = new InputStreamReader(System.in);
            // Store the types stuff in memory
            BufferedReader reader = new BufferedReader(input);
            // Create menu
            output.append(Const.START_WHAT_TO_DO)
                  .append(Const.MENY_START)
                  .append(Const.MENU_EXIT);
            // Display Message
            Helper.f(output.toString(), Const.MENU_GAME_START, 
                                        Const.MENU_QUIT);
            // get user option
            int choice = Integer.parseInt(reader.readLine());
            
            switch(choice)
            {
                case Const.MENU_GAME_START:
                    this.generatePlayers();
                    this.getStartingPlayer(this.getPlayers());
                    this.start();
                    return Const.MENU_GAME_START;
                case Const.MENU_QUIT:
                    Helper.m(Const.BYE_MESSAGE);
                    System.exit(0);
                    return Const.MENU_QUIT;
                default:
                    this.getMainMenu();
                    break;
            }
            
        } catch(IOException | NumberFormatException E)
        {
            this.getMainMenu();
        }
        return Const.MENU_QUIT;
    }
    
    /**
     * Gets the current player turn
     * @return 
     */
    public ArrayList<Player> getTurn()
    {
        return this.turn;
    }
    
    /**
     * Sets the player turn
     * 
     * @param p 
     */
    public void setTurn(Player p)
    {
        if(this.getTurn().isEmpty())
            this.getTurn().add(p);
        else {
            if(!this.getTurn().get(0).equals(p)) {
                this.getTurn().clear();
                this.getTurn().add(p);
            }
        }
    }
    
    /**
     * Get the score board
     * @return 
     */
    public ArrayList<Player> getScoreBoard()
    {
        return this.scoreboard;
    }
    
    /**
     * Adds a player to the core board in the order they finish the game
     * @param p 
     */
    public void addPlayerToScoreBoard(Player p)
    {
        this.getScoreBoard().add(p);
    }
    
    /**
     * Empties the score board
     */
    public void clearScoreBoard()
    {
        this.getScoreBoard().clear();
    }
    
    
    /**
     * Gets the next player in turn
     * 
     * @return 
     */
    public Player getNextPlayer()
    {
        // Get current player from the queue
        if(!this.getTurn().isEmpty())
        {
            // Get the Player
            Player currentPlayer = this.getTurn().get(0);
            // Get player's area5
            Area currentPlayerArea = currentPlayer.getArea();
            // Next area
            Area nextArea  = currentPlayerArea.getNextAreaWithPlayer();
            // return the player
            return nextArea.getOwner();
        }
        
        return null;
    }
    
    /**
     * Each player throws the die and the one that throws
     * the biggest value starts the game. If the player throws a six
     * the others don't get to throw the die anymore.
     * @param players
     */
    public void getStartingPlayer(ArrayList<Player> players)
    {
        // Init a new die
        Die die = new Die();
        // Hold a list of players with equal die value
        ArrayList<Player> equals = new ArrayList(){};
        // Store last player's die value
        int prevPlayerDieValue = 0;
        
        for(Player player : players)
        {
            // Roll the die
            int value = die.roll(player);
            if(value == 6) {
                this.setTurn(player);
                break;
            } else {
                if(value > prevPlayerDieValue)
                {
                    prevPlayerDieValue = value;
                    this.setTurn(player);
                    continue;
                } 
                
                if(value == prevPlayerDieValue && players.indexOf(player) != 0)
                {   
                    int prevPlayerIndex = players.indexOf(player) - 1;
                    equals.add(players.get(prevPlayerIndex));
                    equals.add(player);
                    
                    if(players.indexOf(player) == players.size() - 1)
                    {
                        this.getStartingPlayer(equals);
                        break;
                    }
                }
            }
        }
        
        Helper.f(Const.PLAYER_STARTS_GAME, this.getTurn().get(0));
    }
    
    /**
     * Checks if some game constraints are met
     * @return 
     */
    public boolean gameCanBegin()
    {
        return (this.getPlayers().size() >= 2 && !this.getTurn().isEmpty() && this.getAreas().size() == 4);
    }
    
    /**
     * Getter for learned table
     * @return 
     */
    public double[][] getLearningTable()
    {
        return this.LearningTable;
    }
    
    /**
     * Implements going backwards if the player dice roll would bring her past the last square
     * @param i is the index of the board that potentially may be higher than the board is long
     **/
    private int getLearningTableIndex( int i ) {
	if (i < BOARDLENGTH) { 
	    return i;
	} else {
	    return (LASTSQUARE-(i-LASTSQUARE));
	}
    } 

    /**
     * Prints the learning table 
     **/
    private void printLearningTable() 
    {
	for(int j = 0; j < 6; j++) 
        {
	    for(int i = 0; i < BOARDLENGTH; i++) 
            {
		Helper.f("%.3f", this.getLearningTable()[i][j]); 
	    }
	    Helper.m("");
	}
	Helper.m("");
    }
    
    /**
     * Find the maximum Q-Value for the possible actions in the current state
     * @param i index into q-table in range from 0 to length of board - 1
     * @return The maximum Q-value for index i 
     **/
    private double qForMaxAction( int i ) {
	double max=0;
	for(int j=1;j<6;j++) {
	    if (this.getLearningTable()[i][j]>max) max = this.getLearningTable()[i][j];
	}
	return(max);
    }

    /**
     * This is where Q-learning takes place
     */
    private void learn() 
    {
        // Init die value
	int d;
        // Init reward value
	double reward;
        // Position index in learning table
	int i = 0;

	for(int cnt=0; cnt<ITERATIONS; cnt++) 
        {
            // Simulate die role
	    d = 1 + (int)(Math.random()*6);
            // Convert the value of the die to a 0-index
	    int dieToIndex = d - 1;
	    // Reward init for the last position
	    if ( i+d == LASTSQUARE ) {
		reward = 1; // 1 point given if token lands on last square
	    } else {
		reward = 0; // no points otherwise
	    }	    
            // Calculate the reward value
	    double value = (1-ALPHA)*LearningTable[i][d-1] + ALPHA*(reward + GAMMA * qForMaxAction(this.getLearningTableIndex(i+d))); //q-learning rule
	    // Add the value to the correspondig field index - die value combination
            this.setLearningTableValue(i, dieToIndex, value);
            // move token (backwards if the end i hit)
	    i = this.getLearningTableIndex(i+d); 
            // if land on the last square - return to start = new game
	    if(i == LASTSQUARE) 
                i=0; 
	}
    }
    
    /**
     * Setter for a specific learning table value
     * @param i
     * @param j
     * @param v 
     */
    public void setLearningTableValue(int i, int j, double v)
    {
        this.LearningTable[i][j] = v;
    }
    
    /**
     * Retrieves the reward that corresponds to a specific field with
     * a specific die value
     * 
     * @param die
     * @param fieldIndex
     * @return 
     */
    public double getLearningTableItem(int die, int fieldIndex)
    {
        return this.getLearningTable()[fieldIndex][die];
    }
}
