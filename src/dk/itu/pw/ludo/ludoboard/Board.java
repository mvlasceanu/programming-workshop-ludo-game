/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.pw.ludo.ludoboard
 * @class      GameManager
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.pw.ludo.ludoboard;
import java.util.ArrayList;

/**
 * The gamne manager
 * 
 * @author Mihai-Marius Vlăsceanu
 */
public abstract class Board {
    // The total numeber of fields on the board
    protected int board_size          = 92;
    // The total numeber of fields on the board
    protected static int PATH_SIZE    = 54;
    
    /**
     * Getter for board size
     * @return 
     */
    public int getBoardSize()
    {
        return this.board_size;
    }
    
    /**
     * Setter for board size
     * @param s 
     */
    public void setBoardSize(int s)
    {
        this.board_size = s;
    }
}
