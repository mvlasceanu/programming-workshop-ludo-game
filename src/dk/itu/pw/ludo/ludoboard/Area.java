/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.pw.ludo.ludoboard
 * @class      Area
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.pw.ludo.ludoboard;

import general.Helper;
import java.awt.Color;
import java.util.ArrayList;

/**
 * Area Class
 * @author Mihai-Marius Vlăsceanu
 */
public class Area {
    // Unique identifier for the area
    private String id;
    // Color of the area
    private Color areaColor;
    // Area's owner (Player instance)
    private Player owner;
    // Store area's fields
    private final ArrayList<Field> area_fields = new ArrayList<>();
    // Board 
    protected GameManager Manager;
    
    /**
     * Constructor with a color
     * @param color 
     * @param m 
     */
    public Area(Color color, GameManager m)
    {
        this.areaColor = color;
        this.setId();
        this.setManager(m);
    }
    
    /**
     * Constructor with a player
     * @param p 
     * @param m 
     */
    public Area(Player p, GameManager m)
    {
        this.setOwner(p);
        this.setColor(p.getColor());
        this.setId();
        this.setManager(m);
    }
    
    /**
     * Set area's id
     */
    private void setId()
    {
        this.id = Helper.colorHashCodeToName(this.getColor().hashCode()).substring(0, 1);
    }
    
    /**
     * Get area's id
     * @return 
     */
    public String getId()
    {
        return this.id;
    }
    
    /**
     * Set area's owner
     * @param p 
     */
    public final void setOwner(Player p)
    {
        this.owner = p;
    }
    
    /**
     * Get area's owner
     * @return 
     */
    public Player getOwner()
    {
        return this.owner;
    }
    
    /**
     * Set the color for the area
     * @param c 
     */
    public final void setColor(Color c)
    {
        this.areaColor = c;
    }
    
    /**
     * Get the color of the area
     * @return 
     */
    public Color getColor()
    {
        return this.areaColor;
    }
    
    /**
     * Board setter
     * @param m 
     */
    public final void setManager(GameManager m)
    {
        this.Manager = m;
    }
    
    /**
     * Board getter
     * @return 
     */
    public GameManager getManager()
    {
        return this.Manager;
    }
    
    /**
     * Get all the current area's fields
     * 
     * @return 
     */
    public ArrayList<Field> getFields()
    {
        return this.area_fields;
    }
    
    /**
     * Adds a field to the current area
     * 
     * @param item 
     */
    public void addField(Object item)
    {
        this.getFields().add((Field) item);
    }
    
    /**
     * Drops all fields
     */
    public void dropAllFields()
    {
        this.area_fields.clear();
    }
    
    /**
     * Generates all the fields for the current area
     * 
     * @param board 
     */
    public void generateAreaFields(GameManager board)
    {
        for(int i = 0; i < (board.getBoardSize()/4); i++)
        {
            Field field = new Field(this, i);
            // Corner field (cannot be played)
            if(i == 0)
            {
                field.setFieldType(FieldType.CORNER);
            } else {
                // Home Fields
                if(i <= 4 && i > 0)
                {
                    field.setFieldType(FieldType.HOME);                
                } else {
                    // Start field
                    if(i == 5)
                    {
                        field.setFieldType(FieldType.START);
                    } else {
                        // Finnish field
                        if(i == ((board.getBoardSize()/4) - 1))
                        {
                            field.setFieldType(FieldType.GOAL);
                        } else {
                            
                            // Home column field
                            if((i >= (board.getBoardSize()/4) - 6) && (i < (board.getBoardSize()/4) - 1))
                            {
                                field.setFieldType(FieldType.COLUMN);
                            } else {
                                // Usual field
                                field.setFieldType(FieldType.GAME);
                            }
                        }
                    }
                }
            }
        this.getFields().add(field);
        }
    }
    
    /**
     * Checks if the current area has an owner
     * 
     * @return 
     */
    public boolean hasOwner()
    {
        return this.getOwner() != null;
    }
    
    /**
     * Some name for the area
     * 
     * @return 
     */
    public String getName()
    {
        return Helper.colorHashCodeToName(this.getColor().hashCode());
    }
    
    /**
     * Returns an array with the home fields objects for the area
     * @return 
     */
    public ArrayList<Field> getHomeFields()
    {
        ArrayList<Field> homeFields = new ArrayList<>();
        for(Field f : this.getFields())
        {
            if(f.getFieldType().equals(FieldType.HOME))
                homeFields.add(f);
        }
        return homeFields;
    }
    
    /**
     * Gets the next area to follow
     * 
     * @return 
     */
    public Area getNextArea()
    {
        // Get the areas from the manager
        ArrayList<Area> areas = this.getManager().getAreas();
        // Init a return variable
        Area next = null;
        // Loop through areas
        for(Area a : areas)
        {
            // If the current item in the loop is this area
            if(a == this)
            {
                // Get the current item's index
                int this_id = areas.indexOf(this);
                // If the item is not the last one
                if(this_id != (areas.size() - 1))
                {
                    // The next area is the next one in the collection
                    next = areas.get(this_id + 1);
                } else {
                    // Otherwise, the next area is the first one in the
                    // collection
                    next = areas.get(0);
                }
            }
        }
        return next;
    }
    
    /**
     * Get the area after another area
     * @param a
     * @return 
     */
    public Area getAreaAfter(Area a)
    {
        return a.getNextArea();
    }
    
    /**
     * Gets the next area occupied by a player
     * @return 
     */
    public Area getNextAreaWithPlayer()
    {
        if(this.getAreaAfter(this).getOwner() != null)
            return this.getAreaAfter(this);
        
        Area next = this.getNextArea();
        int i = 0;
        while(next.getOwner() == null)
        {
            if(i == 5)
                break;
            next = next.getNextArea();
            i++;
        }
        return next;
    }
}
