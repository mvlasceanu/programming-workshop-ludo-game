/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.pw.ludo.ludoboard
 * @class      Field
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.pw.ludo.ludoboard;

import general.Helper;
import java.util.ArrayList;

/**
 * Field Class
 * @author Mihai-Marius Vlăsceanu
 */
public class Field {
    // Unique identifier for the field
    private String id;
    // Corresponding area 
    private Area area;
    // Index value for the order
    private int index;
    // Field type (see FieldType.java)
    private FieldType type;
    // Hild the pins that occupy this field
    private final ArrayList<Pin> occupied_by = new ArrayList<>();
    // X position
    private int x;
    // Y position
    private int y;
    
    /**
     * Constructor 
     * @param area
     * @param index 
     */
    public Field(Area area, int index) {
        this.setArea(area);
        this.setIndex(index);
        this.setId();
    }
    
    /**
     * Id setter
     */
    private void setId()
    {
        this.id = this.getArea().getId() + "_" + this.getIndex();
    }
    
    /**
     * Id getter
     * @return 
     */
    public String getId()
    {
        return this.id;
    }
    
    /**
     * Get the field's area
     * @return 
     */
    public Area getArea()
    {
        return this.area;
    }
    
    /**
     * Set field's area
     * @param a 
     */
    public final void setArea(Area a)
    {
        this.area = a;
    }
    
    /**
     * Index getter
     * @return 
     */
    public int getIndex()
    {
        return this.index;
    }
    
    /**
     * Index setter
     * @param i 
     */
    public final void setIndex(int i)
    {
        this.index = i;
    }
    
    /**
     * Field type setter
     * @param type 
     */
    public void setFieldType(FieldType type)
    {
        this.type = type;
    }
    
    /**
     * Returns the type of the field
     * @return 
     */
    public FieldType getFieldType()
    {
        return this.type;
    }
    
    /**
     * Returns the GameManager
     * @return 
     */
    public final GameManager getManager()
    {
        return this.getArea().getManager();
    }
    
    /**
     * Checks if the current field is occupied
     * by another pin
     * 
     * @return 
     */
    public boolean isOccupied()
    {
        return (this.occupied_by.size() > 0);
    }
    
    public Player getOccupiedBy()
    {
        if(this.isOccupied())
            return this.getPins().get(0).getOwner();
        return null;
    }
    
    /**
     * Clears the current field
     */
    public void clear()
    {
        if(this.isOccupied())
        {
            GameManager Manager = this.getManager();
            ArrayList<Position> positions = Manager.getPinPositions();
            for(Position p : positions)
            {
                if(p.getField() == this)
                {
                    boolean remove = positions.remove(this);
                }
            }
        }
    }
    
    /**
     * Returns the pin object that is
     * currently on the field
     * @return 
     */
    public ArrayList<Pin> getPins()
    {
        if(this.isOccupied())
        {
            return this.occupied_by;
        }
        return null;
    }
    
    /**
     * Get the owner of the pins present on the field
     * 
     * @return 
     */
    public Player getPinsOwner()
    {
        if(this.getPins().size() > 0)
        {
            try
            {
                return this.getPins().get(0).getOwner();
            } catch(IndexOutOfBoundsException E)
            {
                Helper.m(E.getMessage());
            }
        }
        return null;
    }
    
}
    