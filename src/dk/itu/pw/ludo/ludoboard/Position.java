/**
 * 
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.pw.ludo.ludoboard
 * @class      Position
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.pw.ludo.ludoboard;
/**
 * Keep pin positions in object
 * @author Mihai-Marius Vlăsceanu
 */
public class Position {
    // Field object
    private Field field;
    // Pin Object
    private Pin pin;
    
    public Position(){};
    /**
     * Constructor
     * @param f Field object
     * @param p Pin Object
     */
    public Position(Field f, Pin p)
    {
        this.setFeild(f);
        this.setPin(p);
    }
    
    /**
     * Field setter
     * @param f 
     */
    public final void setFeild(Field f)
    {
        this.field = f;
    }
    /**
     * Field getter
     * @return 
     */
    public Field getField()
    {
        return this.field;
    }
    /**
     * Pin setter
     * @param p 
     */
    public final void setPin(Pin p)
    {
        this.pin = p;
    }
    /**
     * Pin getter
     * @return 
     */
    public Pin getPin()
    {
        return this.pin;
    }
}
