/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.pw.ludo.ludoboard
 * @class      Player
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.pw.ludo.ludoboard;

import exceptions.UnknownException;
import general.Const;
import general.Helper;
import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Player management class
 * @author Mihai-Marius Vlăsceanu
 */
public class Player {
    // Instance counter
    public static int COUNT_PLAYER_INSTANCES = 0;
    // A unique id for the player
    private int id = 0;
    // A name for the player
    private String name;
    // Player's color
    private Color color;
    // Player's area
    private Area area;
    // Player pins
    private ArrayList<Pin> pins = new ArrayList<>();
    // Store the number of times the player threw 6
    private int bonuses = 0;
    
    /**
     * Constructor
     * @param name
     * @param color 
     */
    public Player(String name, Color color)
    {
        this.generateUniqueId();
        this.setName(name);
        this.setColor(color);
    }
    
    /**
     * Private setter for ID
     * @param id 
     */
    private void setId(int id)
    {
        this.id = id;
    }
    
    /**
     * Getter for player's id
     * @return 
     */
    public final int getId()
    {
        return this.id;
    }
    
    /**
     * Generates aunique ID for the player
     */
    private void generateUniqueId()
    {
        Player.COUNT_PLAYER_INSTANCES++;
        this.setId(Player.COUNT_PLAYER_INSTANCES);
    }
    
    /**
     * Setter for player's color
     * @param c 
     */
    public final void setColor(Color c)
    {
        this.color = c;
    }
    
    /**
     * Getter for player's color
     * @return 
     */
    public final Color getColor()
    {
        return this.color;
    }
    
    /**
     * Setter for player's name
     * @param name 
     */
    public final void setName(String name)
    {
        this.name = name;
    }
    
    /**
     * Getter for player name
     * @return 
     */
    public final String getName()
    {
        return this.name;
    }
    
    /**
     * Assigns the player to an area on the board
     * @param m 
     */
    public void assignArea(GameManager m)
    {
        for(Area a : m.getAreas())
        {
            if(a.getColor() == this.getColor())
            {
                a.setOwner(this);
                this.area = a;
            }
        }
    }
    
    /**
     * Get's current player's area
     * 
     * @return 
     */
    public Area getArea()
    {
        return this.area;
    }
    
    /**
     * Get player's pins
     * @return 
     */
    public ArrayList<Pin> getPins()
    {
        return this.pins;
    }
    
    /**
     * Add pin for current player
     * @param p 
     */
    public void addPin(Pin p)
    {
        this.getPins().add(p);
    }
    
    /**
     * Increase bonuses
     */
    public void addBonus()
    {
        this.bonuses++;
    }
    
    /**
     * Get the number of times the player threw 6
     * @return 
     */
    public int getBonuses()
    {
        return this.bonuses;
    }
    
    /**
     * Bonuses to 0
     */
    public void resetBonuses()
    {
        this.bonuses = 0;
    }
    
    /**
     * Delete all player's pins
     */
    public void deleteAllPins()
    {
        GameManager Manager = this.getArea().getManager();
        Manager.getPinPositions().removeAll(pins);
        this.getPins().removeAll(pins);
    }
    
    /**
     * Creates player's pins
     */
    public final void createPins()
    {
        // Create pin 1
        Pin pin1 = new Pin(this);
        this.getPins().add(pin1);
        pin1.toHomeField();

        // create pin 2
        Pin pin2 = new Pin(this);
        this.getPins().add(pin2);
        pin2.toHomeField();
        
        // Create Pin 3
        Pin pin3 = new Pin(this);
        this.getPins().add(pin3);
        pin3.toHomeField();
        
        // Create pin 4
        Pin pin4 = new Pin(this);
        this.getPins().add(pin4);
        pin4.toHomeField();
        
    }
    
    /**
     * Get the Game Manager
     * @return 
     */
    public GameManager getManager()
    {
        return this.getArea().getManager();
    }
    
    /**
     * Checks if the player can move at least one pin
     * @param die
     * @return 
     */
    public boolean canMove(Die die)
    {
        try {
            // Get current pin positions
            ArrayList<Position> pinPositions = this.getPinPositions();
            // Init move as false
            boolean move = true;
            // Find player's pin positions 
            if(this.getBonuses() > 3)
            {
                // Reset bonuses
                this.resetBonuses();
                // Player can't move
                return false;
            }
            if(this.moveOptions(die).size() == 1)
            {
                return false;
            }
            for(Position p : pinPositions)
            {
                // Get the pin in that position
                Pin pin             = p.getPin();
                // Get the field type in that position
                FieldType type      = p.getField().getFieldType();
                // Get the value of the die the Player threw
                int value           = die.getValue();
                // And get the possible next position according with the die
                Field futurePos     = pin.getNextFieldByDie(die);

                if(futurePos == null)
                {
                    move = false;
                }

                // If the pin is in a home field and he/she didn't threw 6
                if(!type.equals(FieldType.HOME) || value == 6)
                {
                    if(!futurePos.isOccupied()) {
                        move = true;
                    } else {
                        // If the possible next field is occupied
                        if(futurePos.isOccupied()) {
                            // by the player itself
                            if(futurePos.getPinsOwner().equals(this)){
                                // Can'y move there
                                move = false;
                            }
                        }
                    }
                }
            }
            
            return move;
        } catch(NullPointerException E)
        {
            Helper.m(E.getMessage());
        }
        return false;
    }
    
    public int getPinsLeft()
    {
        List<Position> leftPins = new ArrayList<>(this.getManager().getPinPositions());
        for (Iterator<Position> it=leftPins.iterator(); it.hasNext();) {
            if (!it.next().getPin().getOwner().equals(this))
                it.remove(); // NOTE: Iterator's remove method, not ArrayList's, is used.
        }
        
        return leftPins.size();
    }
    
    /**
     * Get player's pins positions
     * @return 
     */
    public synchronized ArrayList<Position> getPinPositions()
    {
        ArrayList<Position> r = this.getManager().getPinPositions();
        ArrayList<Position> temp = new ArrayList<>();
        for(Position ps : r)
        {
            if(ps.getPin().getOwner() == this)
                temp.add(ps);
        }
        return temp;
    }
    
    /**
     * Retrieves all the possible movements that the player can make with the current die
     * value
     * @param d
     * @return 
     */
    public ArrayList<Position> moveOptions(Die d)
    {
        // Init array
        ArrayList<Position> options = new ArrayList<>();
        options.clear();
        // Get player's pin positions (current)
        ArrayList<Position> pos = this.getPinPositions();
        // Add choice 0 as null
        options.add(null);
        
        for (Position ps : pos) {
            // Get the pin in this position
            Pin pin = ps.getPin();
            // Get next field according to the die
            Field next = ps.getPin().getNextFieldByDie(d);
            
            // If the die value doesn't exceed the board size
            if(d.getValue() == 6 && (!next.isOccupied() || !next.getOccupiedBy().equals(pin.getOwner())) && !next.getFieldType().equals(FieldType.GOAL))
            {
                Position possible = new Position(next, pin);
                options.add(possible);
            }
            
            if(d.getValue() != 6 && ps.getField().getFieldType() != FieldType.HOME && (next != null && next.getFieldType() != FieldType.GOAL && (!next.isOccupied() || !next.getOccupiedBy().equals(pin.getOwner()))))
            {
                Position possible = new Position(next, pin);
                options.add(possible);
            }
            
            if(next.getFieldType().equals(FieldType.GOAL))
            {
                Field winning = pin.getPath().get(pin.getPath().size() - 1);
                Position out = new Position(winning, pin);
                options.add(out);
            }
        }
        
        return options;
    }
    
     /**
     * Displays a menu for the player to chose which pins to movr
     * according with the value of the dice
     * 
     * @param e
     * @return 
     */
    public String moveOptionsToString(Die e) throws UnknownException
    {
        try
        {
             // Build a string
            StringBuilder out = new StringBuilder();
            // Move options
            ArrayList<Position> options = this.moveOptions(e);
            
            // Moving options menu message
            out.append(String.format(Const.PLAYER_CAN_MOVE_POS, this.getName()));
            out.append(Const.NEW_LINE);
            
            for(Position pos : options)
            {
                if(pos != null && pos.getField().getFieldType() != FieldType.GOAL)
                {
                    out.append(options.indexOf(pos));
                    out.append(": Pin #");
                    out.append(pos.getPin().getId());
                    out.append(" From field ");
                    out.append(pos.getPin().getCurrentField().getId());
                    out.append(" to field ");
                    out.append(pos.getField().getId());
                    
                    if(pos.getField().isOccupied())
                    {
                        out.append(" [ ");
                        out.append(String.format(Const.KILL_ENEMY, pos.getPin().getOwner().getName(), pos.getPin().getId()));
                        out.append(" ] ");
                    }
                    
                    if(!"".equals(pos.getPin().getOpponentsInProximity()))
                    {
                        out.append(" ( ");
                        out.append(pos.getPin().getOpponentsInProximity());
                        out.append(" ) ");
                    }
                    out.append(Const.NEW_LINE);
                }

                if(pos != null && pos.getField().getFieldType() == FieldType.GOAL)
                {
                    out.append(options.indexOf(pos));
                    out.append(": ");
                    out.append(Const.GET_PIN_OUT);
                    out.append(Const.NEW_LINE);
                }
            }
            // Add Exit option
            out.append(Const.NEW_LINE);
            out.append(Const.MENU_EXIT);
            out.append(Const.NEW_LINE);

            return out.toString();
        } catch( Exception E)
        {
            Helper.m(E.getLocalizedMessage());
        }
        throw new UnknownException("Something Went Wrong. Please try again!");
    }
    
    public boolean move(Die die)
    {
        int choice;
        // Get possible moves
        ArrayList<Position> moptions = this.moveOptions(die);
        GameManager manager = this.getManager();

        if(this instanceof ComputerPlayer) {
            ComputerPlayer cp = (ComputerPlayer) manager.getTurn().get(0);
            choice = (int) cp.decideMove(moptions, die);
        } else {
            try {
                // Display possible moves
                choice =  (int) Helper.ask(this.moveOptionsToString(die), "INT", 0);
            } catch(IOException | ClassCastException | UnknownException e)
            {
                Helper.m(Const.EXC_SELECTION_DNE);
                return this.move(die);
            }
            
        }

        // Proceed with choice
        if(choice != 0 && choice < moptions.size())
        {
            // Get pin from choice
            Pin pin = moptions.get(choice).getPin();
            // Get field
            Field field = moptions.get(choice).getField();
            // Check if the field is occupied
            if(field.isOccupied())
            {
                // Move opponents' pin to home
                if(!field.getPinsOwner().equals(this))
                {
                    Pin opponent = field.getPins().get(0);
                    opponent.killed();
                }
            }
            
            // Move pin
            manager.setPinPosition(field, pin, false);
            // Reset options
            moptions.clear();

            if(die.getValue() != 6 || !this.canMove(die))
                manager.setTurn(manager.getNextPlayer());

        } else {
            if(choice == 0)
            {
                System.exit(0);
            }
            return this.move(die);
        }
        return true;
    }
    
    public double getRewardForPosition(Die die, Pin pin, Field field)
    {
        return this.getManager().getLearningTableItem(die.getValue() - 1, pin.getPath().indexOf(field));
    }
    
    /**
     * Returns the name input by the player with its' color
     * name in parentheses
     * e.g. John (Red)
     * @return 
     */
    @Override
    public String toString()
    {
        return this.getName() + " (" + Helper.colorHashCodeToName(this.getColor().hashCode()) + ") ";
    }
    
    public synchronized boolean isInGame()
    {
        boolean is = false;
        
        for(Player p : this.getManager().getPlayers())
        {
            if(p.equals(this))
                is = true;
        }
        return is;
    }
}
