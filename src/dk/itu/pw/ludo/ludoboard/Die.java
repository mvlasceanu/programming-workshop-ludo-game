/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.pw.ludo.ludoboard
 * @class      Die
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.pw.ludo.ludoboard;

import general.Const;
import general.Helper;

public class Die {
    // Hold the player that throws the die
    private Player player;
    // Hold the value thrown by the player
    private int value;
    
    public Die() {};
   
    /**
     * Generate the die value
     * @param p
     * @return 
     */
    public int roll(Player p)
    {
        this.setPlayer(p);
        int v = (int) (1 + Math.random() * 6);
        this.setValue(v);
        if(this.getValue() == 6)
        {
            this.getPlayer().addBonus();
        } else {
            this.getPlayer().resetBonuses();
        }
        Helper.f(Const.PLAYER_THREW_DIE, this.getPlayer().getName(), this.getValue());
        return this.getValue();
    }
    
    /**
     * Value getter
     * @return 
     */
    public int getValue()
    {
        return this.value;
    }
    
    /**
     * Value setter
     * @param v 
     */
    public final void setValue(int v)
    {
        this.value = v;
    }
    
    /**
     * Player getter
     * @return 
     */
    public Player getPlayer()
    {
        return this.player;
    }
    
    /**
     * Player setter
     * @param p 
     */
    public final void setPlayer(Player p)
    {
        this.player = p;
    }
}
