/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.pw.ludo.ludoboard
 * @class      AI
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.pw.ludo.ludoboard;

import general.Helper;
import java.awt.Color;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class ComputerPlayer extends Player 
{   
    
    public ComputerPlayer(String name, Color color) {
        super(name, color);
    }
    
    public static String getComputerPlayerName(GameManager m)
    {
        ArrayList<String> names = new ArrayList<>();
        String hostname = "Unknown";
        try
        {
            InetAddress addr;
            addr = InetAddress.getLocalHost();
            hostname = addr.getHostName();
        }
        catch (UnknownHostException ex)
        {
            Helper.m("Hostname can not be resolved");
        }
        
        names.add("John");
        names.add("Jane");
        names.add("George");
        names.add("Gabriella");
        names.add(hostname);
        
        int randId = (int) (0 + Math.random() * names.size() - 1);
        String name = names.get(randId);
        
        while(ComputerPlayer.nameExists(name, m))
        {
            names.remove(name);
            randId = (int) (0 + Math.random() * names.size() - 1);
            name = names.get(randId);
        }
        
        return names.get(randId);
    }
    
    public int decideMove(ArrayList<Position> options, Die die)
    {
        int decision = 1;
        double tempRank = 0;
        for(Position pos : options)
        {
            if(pos != null)
            {
                try {
                    Field field = pos.getField();
                    Pin pin = pos.getPin();
                    double rank = this.getRewardForPosition(die, pin, field);
                    if(field.isOccupied() && !field.getPins().get(0).getOwner().equals(pin.getOwner()))
                    {
                        decision = options.indexOf(pos);
                        break;
                    }

                    if(field.getFieldType().equals(FieldType.GOAL)) 
                    {
                        decision = options.indexOf(pos);
                        break;
                    }

                    if(rank > tempRank) {
                        decision = options.indexOf(pos);
                        tempRank = rank;
                    }

                    if(pin.isTheClosestToGoal())
                        decision = options.indexOf(pos);
                } catch (NullPointerException e)
                {
                    Helper.m("Null Exception!");
                }
            }
        }
        return decision;
    }
    
    public static boolean nameExists(String name, GameManager m)
    {
        boolean exists = false;
        for(Player cp : m.getPlayers())
        {
            if(cp.getName().equals(name))
                exists = true;
        }
        return exists;
    }
}
