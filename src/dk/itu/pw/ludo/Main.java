/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.pw.ludo
 * @class      DkItuPwLudo
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */
package dk.itu.pw.ludo;

import dk.itu.pw.ludo.ludoboard.GameManager;
import dk.itu.pw.ludo.ludoboard.Pin;
import dk.itu.pw.ludo.ludoboard.Player;
import java.awt.Color;
import java.io.IOException;
import javax.swing.SwingUtilities;



public class Main {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        
        GameManager Manager = new GameManager();
        Manager.init();
        //SwingUtilities.invokeLater(Manager.getR());
    }
}